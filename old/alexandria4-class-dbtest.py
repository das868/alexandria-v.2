import sys
import PyQt5.QtCore as core
import PyQt5.QtWidgets as widgets
import PyQt5.QtGui as gui
import PyQt5.uic as uic
import sqlite3


class Alexandria:
    def __init__(self):
        print("Started Program................................")
        app = widgets.QApplication(sys.argv)
        window = uic.loadUi("alexandria-buttons-renamed.ui")
        window.showListsButton.clicked.connect(self.handler)
        window.show()
        
        #All code right here...
        db_conn = sqlite3.connect("TestDB.db")
        the_cursor = db_conn.cursor()
        try:
            result = the_cursor.execute("SELECT * FROM employees")
            for row in result:
                print("erstes: ", row[0])
                print("zweites: ", row[1])
            self.displayMessageBox("Did", "it.")
        except sqlite3.OperationalError:
            print("Operation error, maybe DB doesn't exist?")
        except:
            print("Unknown error")
        finally:
            db_conn.commit()
            db_conn.close()
            print("All good 'n' closed again")
            
        sys.exit(app.exec_())

        
    def handler(self):
        #print("It Worked!")
        self.displayMessageBox("It", "Worked")        
        
        
    def displayMessageBox(self, caption, text):
        msg = widgets.QMessageBox()
        msg.setIcon(widgets.QMessageBox.Information) #Information/Warning/Critical
        msg.setText(text)
        msg.setWindowTitle(caption)
        returnval = msg.exec_()


def main():
    #app = widgets.QApplication(sys.argv)
    #w = Window()
    #uic.loadUi("alexandria-buttons-renamed.ui")
    #w.show()
    #handler()
    #w.showListsButton.clicked.connect(handler) # works! don't do the following:!
    #w.centralWidget.gridLayout.showListsButton.clicked.connect(handler)
    #sys.exit(app.exec_())
    a = Alexandria()
    
    
if __name__=="__main__":
    #a = Alexandria()
    main()
