import sys
import PyQt5.QtCore as core
import PyQt5.QtWidgets as widgets
import PyQt5.QtGui as gui
import PyQt5.uic as uic
import sqlite3


class Alexandria:
    def __init__(self):
        print("Started Program................................")
        app = widgets.QApplication(sys.argv)
        window = uic.loadUi("alexandria-buttons-renamed.ui")
        #window.showListsButton.clicked.connect(self.handler)
        window.showListsButton.clicked.connect(self.showAllBooks)
        window.show()
        #All code below here...
        
        #Database schema: id, Titel, Autor, Jahr, Verlag, RK, Tags
        self.db_conn = sqlite3.connect("Alexandria.db")
        the_cursor = self.db_conn.cursor()
        try:
            result = the_cursor.execute("SELECT * FROM bestand")
            for row in result:
                for i in range(0, len(row)-1):
                    print(str(row[i]))
                print(row)
            #self.displayMessageBox("Did", "it.")
        except sqlite3.OperationalError:
            print("Operation error, maybe DB doesn't exist?")
        except:
            print("Unknown error")
        finally:
            self.db_conn.commit()
            self.db_conn.close()
            print("All good 'n' closed again")
        
        #All code above here...
        sys.exit(app.exec_())
               
        
    @property
    def db_conn(self):
        print("Retrieving db_conn...")
        #print(str(self.__db_conn))
        return self.__db_conn
    @db_conn.setter
    def db_conn(self, new_db_conn):
        if new_db_conn is not None:
            print("Setting new db_conn...")
            self.__db_conn = new_db_conn
        else:
            print("Error setting new db_conn...")
    
    
    @property
    def cursor(self):
        print("Retrieving cursor...")
        return self.__cursor
    @cursor.setter
    def cursor(self, new_cursor):
        if new_cursor is not None:
            print("Setting new cursor...")
        else:
            print("Error setting new cursor...")
    
    
    def showAllBooks(self):
        print("Showing all books in MessageBox...")
        result = self.cursor.execute("SELECT * FROM bestand GROUP BY autor")
        restext = ""
        for row in result:
            for i in range(1, len(row)):
                print(row[i-1])
                restext += row[i-1] + "\n"
        self.displayMessageBox("Alle Bücher in bestand:", restext)
    
            
    def handler(self):
        self.displayMessageBox("It", "Worked")        
        print(str(self.db_conn))
        
        
    def displayMessageBox(self, caption, text):
        msg = widgets.QMessageBox()
        msg.setIcon(widgets.QMessageBox.Information) #Information/Warning/Critical
        msg.setText(text)
        msg.setWindowTitle(caption)
        returnval = msg.exec_()


def main():
    #app = widgets.QApplication(sys.argv)
    #w = Window()
    #uic.loadUi("alexandria-buttons-renamed.ui")
    #w.show()
    #handler()
    #w.showListsButton.clicked.connect(handler) # works! don't do the following:!
    #w.centralWidget.gridLayout.showListsButton.clicked.connect(handler)
    #sys.exit(app.exec_())
    a = Alexandria()
    
    
if __name__=="__main__":
    #a = Alexandria()
    main()
