import sys
import PyQt5.QtCore as core
import PyQt5.QtWidgets as widgets
import PyQt5.QtGui as gui
import PyQt5.uic as uic
import sqlite3
import time # for time measurements use t1 - time.time()


def showMessageBox(caption='', text='', kind='Information'):
        msg = widgets.QMessageBox()
        if kind == 'Information':
            msg.setIcon(widgets.QMessageBox.Information)
        elif kind == 'Warning':
            msg.setIcon(widgets.QMessageBox.Warning)
        elif kind == 'Critical':
            msg.setIcon(widgets.QMessageBox.Critical)
        else:
            print("Error: unable to parse desired kind of QMessageBox")
        msg.setText(text)
        msg.setWindowTitle(caption)
        returnval = msg.exec_()


class Alexandria:
    def __init__(self, database_conn):
        # All maim code here
        # create Qt5 application with Qt5 window and appropriate UI
        app = widgets.QApplication(sys.argv)
        window = uic.loadUi("alexandria-buttons-renamed.ui")
        #---------------------TODO: ALL CODE BELOW HERE--------------------
        # initialize fields
        self.database_conn = database_conn
        self.cursor = self.database_conn.cursor()
        # wire up event handling methods for buttons' signals
        # groupBoxListen
        window.addListButton.clicked.connect(self.addListButtonClicked)
        window.delListButton.clicked.connect(self.delListButtonClicked)
        window.showListsButton.clicked.connect(self.showListsButtonClicked)
        # groupBoxMerkzettel
        window.addToNoteButton.clicked.connect(self.addToNoteButtonClicked)
        window.delFromNoteButton.clicked.connect(self.delFromNoteButtonClicked)
        window.showNoteButton.clicked.connect(self.showNoteButtonClicked)
        # groupBoxBestand
        window.addBookButton.clicked.connect(self.addBookButtonClicked)
        window.delBookButton.clicked.connect(self.delBookButtonClicked)
        window.massAddButton.clicked.connect(self.massAddButtonClicked)
        window.searchBookButton.clicked.connect(self.searchBookButtonClicked)
        #groupBoxLeihe
        window.addLentForeignButton.clicked.connect(self.addLentForeignButtonClicked)
        window.addLentOwnButton.clicked.connect(self.addLentOwnButtonClicked)
        window.backLentForeignButton.clicked.connect(self.backLentForeignButtonClicked)
        window.backLentOwnButton.clicked.connect(self.backLentOwnButtonClicked)
        
        
        #---------------------TODO: ALL CODE ABOVE HERE--------------------
        window.show()
        sys.exit(app.exec_())
        
        
    # Custom destructor to ensure database_conn gets closed finally
    def __del__(self):
        self.database_conn.commit()
        self.cursor = None
        self.database_conn.close()
        self.database_conn = None
        print("Destructor fo Alexandria object done!")
    
    
    # Make the database connection and the belonging cursor a 
    # class-wide accessible property
    @property
    def database_conn(self):
        print("Getter for database_conn...")
        return self.__database_conn
    @database_conn.setter
    def database_conn(self, new_db_conn):
        print("Setter for database_conn...")
        self.__database_conn = new_db_conn
    
    
    @property
    def cursor(self):
        print("Getter for cursor...")
        return self.__cursor
    @cursor.setter
    def cursor(self, new_cursor):
        print("Setter for cursor...")
        self.__cursor = new_cursor
        
    
    # Action SLOTS and event handling methods
    # groupBoxListen
    # TODO: addListButtonClicked braucht eigenen 2. Dialog für custom Attr.!
    def addListButtonClicked(self):
        print("\naddListButtonClicked Worked")
        showMessageBox("Désolé", "NotImplementedException")
        pass
        # dialog = uic.loadUi("add+delListDialog.ui")
        # dialog.setWindowTitle("Neue Liste hinzufügen...")
        # dialog.groupBox.setTitle("Neue Liste hinzufügen")
        # returnval = dialog.exec_()
        # print("addListDialog to add list exited with code ", returnval) #OK=1, CANCEL=0
        # if returnval == 1:
        #     liste = str(dialog.lineEditListe.text())
        #     print("User said OK and entered list name: ", liste)
        #     dialog2 = uic.loadUi("editMerkzettelDialog2.ui")
        #     rv = dialog2.exec_()
        #     #self.cursor.execute("CREATE TABLE............")
        # elif returnval == 0:
        #     print("User said CANCEL")
        # else:
        #     print("Unknown error, addListDialog return failed")
        # del dialog
        # print("Destructor for addListDialog done!")
    
    
    def delListButtonClicked(self):
        print("\ndelListButtonClicked Worked")
        whitelist = ['listen', 'bestand', 'merkzettel', 'verliehen', 'erliehen']
        dialog = uic.loadUi("add+delListDialog.ui")
        returnval = dialog.exec_()
        print("delListDialog exited with code ", returnval) #OK=1, CANCEL=0
        liste = dialog.lineEditListe.text()
        if returnval == 1:
            print("User said OK and entered list name: ", 
                  str(liste))
            if not (liste in  whitelist):
                warning_text = "Wollen Sie Ihre Liste \"{}\" wirklich " \
                            "unwiderruflich löschen?".format(str(liste))
                msg = widgets.QMessageBox()
                msg.setIcon(widgets.QMessageBox.Warning)
                msg.setWindowTitle("Warnung!")
                msg.setText(warning_text)
                msg.setStandardButtons(widgets.QMessageBox.Yes)
                msg.addButton(widgets.QMessageBox.No)
                msg.setDefaultButton(widgets.QMessageBox.No)
                returnval = msg.exec_()
                print("returnval of warning QMessageBox: ", str(returnval))
                if returnval == 16384:
                    try:
                        print("==> Deleting custom list {}".format(str(liste)))
                        self.cursor.execute("DROP TABLE {}".format(str(liste)))
                        self.cursor.execute("DELETE FROM listen WHERE table_name "\
                                            "= \'{}\';".format(str(liste)))
                    except sqlite3.OperationalError:
                        print("==> Error table {} doesn't exist".format(str(liste)))
                        showMessageBox("Fehler", "Liste \"{}\" existiert nicht."
                                       .format(str(liste)), 'Critical')
                    except:
                        print("==> Unknown Error in delListDialog")
                        showMessageBox("Unbekannter Fehler", "Konnte Aktion "\
                                "nicht ausführen. Bei wiederholtem Auftreten "\
                                "bitte Administrator verständigen", 'Critical')
                elif returnval == 65536:
                    print("==> Deletion aborted by user")
                else:
                    print("==> Unknown error in list deletion confirmation")
            else:
                whiteliststr = '\n' + (str(whitelist)[1:-1]).replace('\'', '') + '.'
                showMessageBox("Error deleting list", "You can only delete " \
                               "lists created by your own, but not:{}"
                               .format(whiteliststr), 'Critical')
        elif returnval == 0:
            print("User said CANCEL")
        else:
            print("Unknown error, delListDialog return failed")
        del dialog
        print("Destructor for delListDialog done!")
    
    
    def showListsButtonClicked(self):
        print("\nshowListsButtonClicked Worked")
        # Sample query
        print("Getting table names......")
        result = self.cursor.execute("SELECT table_name FROM listen;")
        resstr=""
        for row in result:
            resstr += '- '
            for i in range(0, len(row)):
                resstr += str(row[i])
            resstr += '\n'
        n = resstr.find('- erl')
        showMessageBox("Präsente Listen", "Voreingestellt:\n" + 
                       resstr[:n] + '\nBenutzerdefiniert:\n' + resstr[n:])
    
    
    # groupBoxMerkzettel
    def addToNoteButtonClicked(self):
        print("\naddToNoteButtonClicked worked")
        dialog = uic.loadUi("editMerkzettelDialog.ui")
        dialog.lineEditJahr.setText("-----")
        #dialog.lineEditJahr.setReadOnly(True)
        dialog.labelJahr.setDisabled(True)
        dialog.lineEditJahr.setDisabled(True)
        returnval = dialog.exec_()
        if returnval == 1:
            print("User said OK and entered Jahr: ", str(dialog.lineEditJahr.text()))
        elif returnval == 0:
            print("User said CANCEL")
        else:
            print("Unknown error, addToNoteDialog return failed")
        del dialog
        print("Destructor for editMerkzettelDialog done!")
    
    
    def delFromNoteButtonClicked(self):
        print("delFromNoteButtonClicked worked")
        pass
    
    
    def showNoteButtonClicked(self):
        print("showNoteButtonClicked worked")
        pass
    
    
    # groupBoxBestand
    def addBookButtonClicked(self):
        print("addBookButtonClicked worked")
        pass
    
    
    def delBookButtonClicked(self):
        print("delBookButtonClicked worked")
        pass
    
    
    def massAddButtonClicked(self):
        print("massAddButtonClicked worked")
        pass
    
    
    def searchBookButtonClicked(self):
        print("searchBookButtonClicked worked")
        pass
    
    
    #groupBoxLeihe
    def addLentForeignButtonClicked(self):
        print("addLentForeignButtonClicked worked")
        pass
    
    
    def addLentOwnButtonClicked(self):
        print("addLentOwnButtonClicked worked")
        pass
    
    
    def backLentForeignButtonClicked(self):
        print("backLentForeignButtonClicked worked")
        pass
    
    
    def backLentOwnButtonClicked(self):
        print("backLentOwnButtonClicked worked")
        pass
    
    
def main():
    # Database schema: id, Titel, Autor, Jahr, Verlag, RK, Tags
    a = Alexandria(sqlite3.connect("Alexandria-all-tables.db"))
    del a


if __name__ == "__main__":
    main()