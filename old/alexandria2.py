import sys
import PyQt5.QtCore as core
import PyQt5.QtWidgets as widgets
import PyQt5.QtGui as gui
import PyQt5.uic as uic
import sqlite3


def handler():
    #print("It Worked!")
    msg = widgets.QMessageBox()
    msg.setIcon(widgets.QMessageBox.Information)
    msg.setText("Worked.")
    msg.setWindowTitle("It")
    returnval = msg.exec_()


app = widgets.QApplication(sys.argv)
w = uic.loadUi("alexandria-buttons-renamed.ui")
w.show()
handler()
w.showListsButton.clicked.connect(handler) # works! don't do the following:!
#w.centralWidget.gridLayout.showListsButton.clicked.connect(handler)

sys.exit(app.exec_())
